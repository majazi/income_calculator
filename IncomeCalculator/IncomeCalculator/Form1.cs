﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace IncomeCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            //read tax brackets from csv
            using (var reader = new StreamReader(@"C:\Users\Genesis\Desktop\Projects\Income_Calculator\taxFile.csv"))
            {
                List<double> taxRate = new List<double>();
                List<double> startBraket = new List<double>();
                List<double> endBraket = new List<double>();
                var stateTax = 0.0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    taxRate.Add(Convert.ToDouble(values[0]));
                    startBraket.Add(Convert.ToDouble(values[1]));
                    endBraket.Add(Convert.ToDouble(values[2]));
                    stateTax = Convert.ToDouble(values[3]);
                }
                //set max taxes for each bracket
                List<double> payedTaxPerBracket = new List<double>();
                double totalMoney = Convert.ToDouble(textBox1.Text) - Convert.ToDouble(textBox2.Text); //total money minus pre-tax deductions
                double remainingMoney = 0.00;
                double taxOfRemainingMoney = 0.00;

                //so sloppy :) <- Past Michael not helpful!!!
                //set the for loop start and end point in these statemnts
                if (comboBox1.Text == "Single")
                {
                    payedTaxPerBracket.Add(0.00);
                    payedTaxPerBracket.Add(taxRate[0] * (endBraket[0]));
                    payedTaxPerBracket.Add(taxRate[1] * (endBraket[1] - endBraket[0]) + payedTaxPerBracket[1]);
                    payedTaxPerBracket.Add(taxRate[2] * (endBraket[2] - endBraket[1]) + payedTaxPerBracket[2]);
                    payedTaxPerBracket.Add(taxRate[3] * (endBraket[3] - endBraket[2]) + payedTaxPerBracket[3]);
                    payedTaxPerBracket.Add(taxRate[4] * (endBraket[4] - endBraket[3]) + payedTaxPerBracket[4]);
                    payedTaxPerBracket.Add(taxRate[5] * (endBraket[5] - endBraket[4]) + payedTaxPerBracket[5]);
                    payedTaxPerBracket.Add(taxRate[6] * (endBraket[6] - endBraket[5]) + payedTaxPerBracket[6]);

                    for (int i = 0; i < 6; i++)
                    {
                        if (totalMoney < endBraket[i])
                        {
                            taxOfRemainingMoney = ((totalMoney - startBraket[i]) * taxRate[i]) + payedTaxPerBracket[i];
                            calculateTaxes(totalMoney, remainingMoney, taxOfRemainingMoney, stateTax);
                            break;
                        }
                    }
                }
                else
                {
                    payedTaxPerBracket.Add(0.00);
                    payedTaxPerBracket.Add(taxRate[7] * (endBraket[7]));
                    payedTaxPerBracket.Add(taxRate[8] * (endBraket[8] - endBraket[7]) + payedTaxPerBracket[1]);
                    payedTaxPerBracket.Add(taxRate[9] * (endBraket[9] - endBraket[8]) + payedTaxPerBracket[2]);
                    payedTaxPerBracket.Add(taxRate[10] * (endBraket[10] - endBraket[9]) + payedTaxPerBracket[3]);
                    payedTaxPerBracket.Add(taxRate[11] * (endBraket[11] - endBraket[10]) + payedTaxPerBracket[4]);
                    payedTaxPerBracket.Add(taxRate[12] * (endBraket[12] - endBraket[11]) + payedTaxPerBracket[5]);
                    payedTaxPerBracket.Add(taxRate[13] * (endBraket[13] - endBraket[12]) + payedTaxPerBracket[6]);

                    for (int i = 7; i < 13; i++)
                    {
                        if (totalMoney < endBraket[i])
                        {
                            taxOfRemainingMoney = ((totalMoney - startBraket[i]) * taxRate[i]) + payedTaxPerBracket[i-7];
                            calculateTaxes(totalMoney, remainingMoney, taxOfRemainingMoney, stateTax);
                            break;
                        }
                    }
                }

            }
        }
        public void calculateTaxes(double totalMoney, double remainingMoney, double  taxOfRemainingMoney, double stateTax)
        {
            double result = 0.00;
            double stateTaxAmmount = totalMoney * stateTax;
            double socialSecurityTax = totalMoney * 0.062;
            double medicareTax = totalMoney * 0.0145;

            result  = totalMoney - taxOfRemainingMoney - stateTaxAmmount - socialSecurityTax - medicareTax - Convert.ToDouble(textBox3.Text);

            label1.Text = result.ToString();
        }
    }
}
